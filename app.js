// const express = require("express");
import express from "express";
// const http = require("http");
import http from 'http';
import { fileURLToPath } from "url";
import json from "body-parser";
import path from "path";
import misRutas from './router/index.js'

const puerto = 80;
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

//declara la variable punto de inicio
const main = express();
console.log(misRutas.router)
main.set("view engine", "ejs");
main.set(express.static(__dirname + '/public'));
main.use(json.urlencoded({ extendeds: true }));
main.use(misRutas.router);

//declarar primera ruta, ruta por omision

main.listen(puerto, () => {
    console.log("Se inicio el servidor pro el puerto: " + puerto)

})