import express, { Router } from "express";
import json from "body-parser";
import alumnosDb from "../models/alumnos.js";

export const router = express.Router();

router.get('/', (req, res) => {
    res.render('index', { titulo: "Mis practicas js", nombre: "Abel Sanchez Urrea" })
})
router.get('/table', (req, res) => {
    //parametros
    const params = {
        numero: req.query.numero
    }
    res.render('table', params);
})
router.post('/table', (req, res) => {
    //parametros
    const params = {
        numero: req.body.numero
    }
    res.render('table', params);
})
router.get('/cotizacion', (req, res) => {
    const params = {
        valor: req.query.valor,
        pinicial: req.query.pinicial,
        plazo: req.query.plazos,
        newValues: req.query.newValues
    }
    console.log(params.plazo)
    res.render('cotizacion', params);
})
router.post('/cotizacion', (req, res) => {
    const params = {
        valor: req.body.valor,
        pinicial: req.body.pinicial,
        plazo: req.body.plazos,
        newValues: req.body.newValues
    }
    res.render('cotizacion', params);
})
let rows;
router.get('/alumnos', async (req, res) => {
    rows = await alumnosDb.mostarDatos();
    res.render('alumnos', { reg: rows })
})
let params;
router.post('/alumnos', async (req, res) => {
    try {
        params = {
            matricula: req.body.matricula,
            nombre: req.body.nombre,
            domicilio: req.body.domicilio,
            sexo: req.body.sexo,
            especialidad: req.body.especialidad
        }
        const res = await alumnosDb.insertar(params);


    } catch (error) {
        console.log(error);
        res.status(400).send("Sucedio un error" + error);
    }
    // res.render('alumnos', params);
    rows = await alumnosDb.mostarDatos();
    res.render('alumnos', { reg: rows })
})

export default { router }