import mysql from 'mysql2'

var conexion = mysql.createConnection({
    host: "127.0.0.1",
    user: "root",
    password: 'root',
    database: "sistemas"
})

conexion.connect(function (error) {
    if (error) {
        console.log('Error' + error);
        return;
    }
    console.log("Se abrio la conexion a con exito")
})

export default conexion;