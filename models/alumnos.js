import { resolve } from "path";
import conexion from "./conexion.js";
import { rejects } from "assert";



var alumnosDb = {};

alumnosDb.insertar = function insertar(alumno) {
    return new Promise((resolve, rejects) => {

        //Consulta
        let slqConsulta = "INSERT INTO alumnos SET ?";
        conexion.query(slqConsulta, alumno, function (err, res) {
            if (err) {
                console.log("Surgio un error" + err.message);
                rejects(err);
            } else {
                const alumno = {
                    id: res.id,
                }
                resolve(alumno);
            }
        })

    })
}
alumnosDb.mostarDatos = function mostarTodos() {
    return new Promise((resolve, rejects) => {
        let sqlConsulta = "SELECT * FROM alumnos";
        conexion.query(sqlConsulta, null, function (err, res) {
            if (err) {
                console.log("surgio un error");
                rejects(err);
            } else {
                resolve(res);
            }
        })
    })
}
export default alumnosDb;
